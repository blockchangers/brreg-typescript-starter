import { StockFactory, EntityRegistry, RegistryOfCapTables } from "@brreg/sdk";
import { ethers } from 'ethers'

// npm run dev
const run = async (): Promise<unknown> => {
    console.log("Running");

    const provider = new ethers.providers.JsonRpcProvider('http://ethjygmk4-dns-reg1.northeurope.cloudapp.azure.com:8540')
    const ProviderWithWallet = ethers.Wallet.createRandom().connect(provider)
    const walletAddress = await ProviderWithWallet.getAddress()
    const id = (getRandomInt(9, 999999)).toString();
    console.log("Wallet => ", walletAddress);
    console.log("id => ", id);

    function getRandomInt(min: number, max: number): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const list = async () => {
        const RegistryOfCapTablesContract = await RegistryOfCapTables.init(ProviderWithWallet);
        const list = await RegistryOfCapTablesContract.list();
        console.log("list", list);
    }
    //list()

    const createCompany = async () => {
        const companyFactory = await StockFactory.init(ProviderWithWallet);
        const Company = await companyFactory.createNew("Blockchangers AS", "915772137");
        console.log("Company created with name: ", await Company.name());
    }
    //createCompany()

    const allTransactionsAllCapTables = async () => {
        const entityRegistry = await EntityRegistry.init(ProviderWithWallet);
        let allTransactionsAllCapTables = await entityRegistry.allTransactionsAllCapTables("195199646563");
        console.log(allTransactionsAllCapTables);
    }
    //allTransactionsAllCapTables()

    const addEntity = async () => {
        const entityRegistry = await EntityRegistry.init(ProviderWithWallet)
        console.log("Adding entity");
        const addEntityTx = await entityRegistry.addEntity({
            address: walletAddress,
            uuid: id,
            type: 'person', // or 'organization
            name: "Testperson",
            country: "Norway",
            city: "Oslo",
            postalcode: "0179",
            streetAddress: "Fiberbekken 6"
        });
        await addEntityTx.wait()
        console.log("Waiting for entity");
        const entityFromBlockchain = await entityRegistry.getEntityByUuid(id)
        console.log("entityFromBlockchain", entityFromBlockchain.address);

    }
    //addEntity()

    const issueAndTransfer = async () => {

        // Must run addEntity before
        console.log("Starting create company");
        const companyFactory = await StockFactory.init(ProviderWithWallet);
        const Company = await companyFactory.createNew("Blockchangers AS", "915772137");
        console.log("Company created with name: ", await Company.name());
        let issueTx = await Company.issue(id, 1000);
        await issueTx.wait()
        console.log("Issue complete");
        const balance1 = await Company.balanceOf(walletAddress)
        console.log("balance is ", balance1);
        let transferTx = await Company.transfer('195199646563', 500);
        console.log("Transfer started");
        await transferTx.wait()
        console.log("transfer done");
        const balance2 = await Company.balanceOf(walletAddress)
        console.log("balance is ", balance2);
    }
    //issueAndTransfer()

    return setTimeout(() => {
        console.log("Exiting");
    }, 9999999)
}
run()

export { }